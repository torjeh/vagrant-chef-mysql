#
# Cookbook Name:: nst-mysql
# Recipe:: default
#
# Copyright 2015, NST 
#
# All rights reserved - Do Not Redistribute
#

mysql_service 'nst' do
        version '5.5'
        initial_root_password 'nst'
        action [:create, :start]
end

